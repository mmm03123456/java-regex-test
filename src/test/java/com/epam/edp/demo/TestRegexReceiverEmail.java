package com.epam.edp.demo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class TestRegexReceiverEmail {

    private static final String[] emails = {"autocode@epam.com", "example2@example.com"};
    private static final String[] nonEmails = {"autocode.epam.com", "@example.com", "", "2"};

    @Test
    public void isRegexShort() {
        assertTrue(RegexReceiver.EMAIL_PATTERN_STRING.length() < 15);
    }

    @Test
    public void isEmailTest() {

        for (String email : emails) {
            assertTrue(RegexReceiver.isEmail(email));
        }

        for (String nonEmail : nonEmails) {
            assertFalse(RegexReceiver.isEmail(nonEmail));
        }
    }

}
